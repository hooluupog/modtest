package main

import (
	"fmt"

	"gitlab.com/hooluupog/DataStructure/stack"
)

func main() {
	s := new(stack.Stack)
	s.Push(1)
	s.Push(2)
	s.Push(3)
	s.Pop()
	s.Push(4)
	fmt.Println(s)
}
